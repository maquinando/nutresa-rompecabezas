
var messages = {
    start: {
        title: null,
        btn: '¡A jugar!',
        msg: 'En este juego encontrarás una serie de rompecabezas que debes armar ubicando las fichas correctamente. Comienza a jugar y conoce más sobre cada uno de los procesos.'
    },
    end: {
        title: '¡MUY BIEN!',
        btn: null,
        msg: 'Lograste completar de manera correcta los procesos que aplicamos dentro del Negocio y cómo se conectan entre ellos.',
    },
    pause: {
        title: null,
        btn: null,
        msg: '<b>¡Juego en pausa!</b>.',
    }
};
var gameEnded = false;
var isComplete = false;
var paused = false;
var currentPuzzle = 0;
var selector = '#puzzle-canvas-wrapper';
var messageCallback = null;
var piecesSize = 'big';

jQuery(document).ready(function($) {
	


	function nextPuzzle() {
		if(currentPuzzle == 6) {
			this.showMessage(messages.end.msg, onGameEnded);
		}
		var isComplete = false;
		currentPuzzle++;
		$(selector).empty();
		var _src = 'img/puzzle_' + currentPuzzle + '.jpg';
		var puzzle = jqJigsawPuzzle.createPuzzleFromURL(selector, _src, {
			piecesSize: piecesSize, // normal, big o small
			borderWidth: 0, // el borde al rededor de cada pieza
			shuffle: { // define el área donde deben quedar las piezas.
				rightLimit : -280,
				leftLimit : -280,
				bottomLimit : -150,
				topLimit : -150
			}, 
			onCreated: function() {
				organizeShapes();
			},
			callback: function() {

				showFullImage(function() {
					nextPuzzle();
				})
			},
			container: selector
		});
	}
	function organizeShapes() {

		$('#helper').empty();
		$(selector).find('.piece').each(function() {
			var helperClass = this.className.split(' ')[2].replace('piece', 'helper');
			var $helper = $('<div>').addClass(piecesSize).addClass('helper').addClass(helperClass);
			var posX = $(this).data('posx');
			var posY = $(this).data('posy');
			var width = $(this).width();
			var height = $(this).height();
			var backgroundImage = $(this).css('background-image');
			var backgroundPos = $(this).css('background-position');
			
			$helper.css('background-image', backgroundImage);
			$helper.css('background-position', backgroundPos);
			$helper.css('width', width-2);
			$helper.css('height', height-2);

			$wrapper = $('<div class="helper-wrapper">');
			$wrapper.addClass(piecesSize).addClass(helperClass);
			$wrapper.css('width', width);
			$wrapper.css('height', height);
			$wrapper.css('left', posX);
			$wrapper.css('top', posY);

			$wrapper.append($helper);
			$('#helper').append($wrapper);
		})
		$('#pile').empty();
		$(selector)
			.find('.piece')
			.sort(function(a, b){return 0.5 - Math.random()})
			.each(function() {
				var piece = $(this).clone();
				piece.removeClass("ui-draggable");
				piece.addClass("piece-mock"); 
				piece.removeAttr("id");

				piece.attr('target', '#' + $(this).attr('id'));

				$('#pile').append(piece);
				$(this).addClass('hidden');

				piece.on('click mousedown touchstart', function(){
					var _target = $(this).attr('target');
					var piece = $(_target);
					$(this).remove();
					piece.removeClass('hidden');
				})

			});
	}
	$('#show-full-image-button').on('click', function(e) {
		showFullImage();
		e.preventDefault();
	});
	function showFullImage(callback) {
		$.colorbox({
			href: 'img/puzzle_' + currentPuzzle + '.jpg', 
			height: "80%", 
			width: "80%",
			onComplete: function() {
				// $('.cboxPhoto').zoom({url: 'img/full/puzzle_' + currentPuzzle + '.jpg'});
				$('#cboxLoadedContent img')
				.wrap('<span style="display:inline-block"></span>').
				css('display', 'block').parent()
				.zoom({
					url: 'img/full/puzzle_' + currentPuzzle + '.jpg'
				});
			}, 
			onClosed: function() {
				if( callback ) callback();
			}
		});
	}
	function pause(doNotShowMessage) {
		paused = true;
		if(!doNotShowMessage) showMessage(messages.pause);
	}
	function setupModals() {
		$('#modal-close-button, .modal-play-button').on('click', function(){
            $("#message-modal-box-wrapper").fadeOut(200,function(){
                $("#message-modal-box, #start-message-modal-box, #message-final-box").css("display","none");
				paused = false;
                if(messageCallback) messageCallback();
            });
        });
        $('#modal-final-close-button').on('click', function() {
            $("#message-modal-box-wrapper").fadeOut(200,function(){
                $("#message-modal-box, #start-message-modal-box, #message-final-box").css("display","none");
                paused = false;
                if(messageCallback) messageCallback();
            });
        });
        $(window).resize(function(){

        });
        showMessage(messages.start);
	}

	function showMessage(msg) {
		if($('#message-final-box').is(':visible') || $('#start-message-modal-box').is(':visible'))
            return;
        $modalBox = $('#message-modal-box');
        if(!msg.title || msg.title == '') {
            $modalBox.find('#title-word').hide();
        } else {
            $modalBox.find('#title-word').show().html(msg.title);
        }
        if(!msg.msg || msg.msg == '') {
            $modalBox.find('#related-message').hide();
        } else {
            $modalBox.find('#related-message').show().html(msg.msg);
        }
        if(!msg.btn || msg.btn == '') {
            $modalBox.find('#related-button').hide();
        } else {
            $modalBox.find('#related-button').show().html(msg.btn);
        }
        
        $('#message-modal-box').css('display', 'inline-block');
        $('#message-modal-box-wrapper').fadeIn('slow');
	}
	function onGameEnded() {
		// Callback para cuando el juego termine
	}
	setupModals();
	nextPuzzle();
});
