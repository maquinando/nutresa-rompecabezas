
!function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);


var game_status = {
    started: false,
    ended: false,
    level: 0,
    lines: 0,
    paused: false,
    sound: true,
    speed: 10,
    gameOver: false
};
var d = new Date();
game_status.startTime = d.getTime();




var messages = {
    start: {
        title: null,
        btn: '¡A jugar!',
        msg: 'En este juego encontrarás una serie de rompecabezas que debes armar ubicando las fichas correctamente. Comienza a jugar y conoce más sobre cada uno de los procesos.'
    },
    end: {
        title: '¡MUY BIEN!',
        btn: null,
        msg: 'Lograste completar de manera correcta los procesos que aplicamos dentro del Negocio y cómo se conectan entre ellos.',
    },
    pause: {
        title: null,
        btn: null,
        msg: '<b>¡Juego en pausa!</b>.',
    }
};
var puzzles = [{
    img: 'assets/img/puzzle_1.jpg',
},{
    img: 'assets/img/puzzle_2.jpg'
},{
    img: 'assets/img/puzzle_3.jpg'
},{
    img: 'assets/img/puzzle_4.jpg'
},{
    img: 'assets/img/puzzle_5.jpg'
},{
    img: 'assets/img/puzzle_6.jpg'
}];

(function($) {
    var $game;
    var oldLine = null;
    var oldLevel = null;
    var nextMessage = null;

    var _lineMsg = 0;
    var _nextMessageTimeout = 20 * 1000;
    var _nextMessageTimer = null;

    var GameController = function(selector) {

        // this._audioVolume = 0.4;
        // this.audio = new Audio('assets/audio/Jingle.mp3');
        // this.audio.addEventListener('load', function(event) {
        //     audio.volume = _audioVolume;
        // });
        // this.audio.volume = 0.2;


        this.columns = 7;
        this.rows = 4;

        this.started  = false;
        this.ended  = false;
        this.level  = 0;
        this.lines  = 0;
        this.paused  = false;
        this.sound  = true;
        this.speed  = 10;
        this.gameOver  = false;

        this.wasPaused = false;
        this.paused = false;

        this.wasSound = false;
        this.sound = false;

        this.interval = 60;

        this.selector = $(selector);
        this.currentPuzzleIndex = 0;
        this.currentPuzzle = puzzles[0];

        this.startPuzzle();
        var _this = this;
        setInterval(this.gameUpdate, 90);


        

        
        $(window).blur(function(){
            if(!_this.paused)
                _this.pause();
        });


    }
    GameController.prototype.restart = function() {
        this.currentPuzzle = 1;
        this.startPuzzle();
    }
    GameController.prototype.startPuzzle = function() {
        $('#puzzle-image').snapPuzzle('destroy');
        if(this.currentPuzzleIndex == 0) {
            this.showMessage(messages.start);
        } else if(this.currentPuzzleIndex > puzzles.length -1) {
            this.showMessage(messages.end);
        }
        this.currentPuzzle = puzzles[this.currentPuzzleIndex];
        var $img = $( '<img />' );
        $img.attr('src', this.currentPuzzle.img);
        $img.attr('id', 'puzzle-image');
        this.selector.html($img);
        
        var _this = this;
        $img.on('load', function() {
            setTimeout(function() {

                $('#pile').height($('#puzzle-image').height());
                $('.restart-puzzle').click(function(){
                    start_puzzle($(this).data('grid'));
                });
               

               

                $('#puzzle_solved').hide();
                _this.game =$('#puzzle-image').snapPuzzle({
                    rows: _this.rows, 
                    columns: _this.columns,
                    pile: '#pile',
                    containment: '#puzzle-containment',
                    onComplete: function(){
                        $('#source_image').fadeOut(150).fadeIn();
                        $('#puzzle_solved').show();
                        setTimeout(function() {
                            _this.nextPuzzle();
                        }, 2000)
                    }
                });
                _this.started  = true;
            }, 1000)
        });

    }
    GameController.prototype.pause = function() {
        //your code here
        this.paused = true;
        this.showMessage(messages.pause);
    }
    GameController.prototype.nextPuzzle = function() {
        this.currentPuzzleIndex ++;
        this.startPuzzle();
    }
    GameController.prototype.showStartMessage = function() {
        /* Start automatically */
        this.paused = true;
        $("#start-message-modal-box").css("display","block");
        $("#message-modal-box-wrapper").fadeIn(200);
    }
    GameController.prototype.showMessage = function(msg, callback) {
        if(callback) {
            messageCallback = callback;
        } else {
            messageCallback = null;
        }

        if($('#message-final-box').is(':visible') || $('#start-message-modal-box').is(':visible'))
            return;
        this.paused = true;
        $modalBox = $('#message-modal-box');
        if(!msg.title || msg.title == '') {
            $modalBox.find('#title-word').hide();
        } else {
            $modalBox.find('#title-word').show().html(msg.title);
        }
        if(!msg.msg || msg.msg == '') {
            $modalBox.find('#related-message').hide();
        } else {
            $modalBox.find('#related-message').show().html(msg.msg);
        }
        if(!msg.btn || msg.btn == '') {
            $modalBox.find('#related-button').hide();
        } else {
            $modalBox.find('#related-button').show().html(msg.btn);
        }
        
        $('#message-modal-box').css('display', 'inline-block');
        $('#message-modal-box-wrapper').fadeIn('slow');
    }

    GameController.prototype.onGameOver = function(score) {
        if($('#message-final-box').is(':visible') || $('#start-message-modal-box').is(':visible')) {
            $('#message-final-box').hide();
            $('#start-message-modal-box').hide();
        }

        // game_status.ended = true;
        showMessage("Intentalo de nuevo","Juega nuevamente y podrás descubrir la importancia de la <strong>gestión por procesos.</strong>");
        game_status.gameOver = true;
        
        if(_nextMessageTimer != null) {
            clearTimeout(_nextMessageTimer);
        }
        _nextMessageTimer = null;
        
        // onGameEnded();
    }
    GameController.prototype.gameUpdate = function() {
        if(!this.started) return;
        if(this.wasPaused && !this.paused) {
        }
        else if(!this.wasPaused && this.game_status.paused) {
        }
        this.wasPaused = this.paused;
        if(this.wasSound && !this.sound) {
        }
        else if(!this.wasSound && this.sound) {
        }
        this.wasSound = this.sound;


    }



    function showNextMessage() {
        if(game_status.ended || game_status.paused) return;
        if( typeof messages[_lineMsg] == 'undefined') 
            return;
        showMessage('', messages[_lineMsg].message);
        _lineMsg ++;

        if(_lineMsg >= messages.length) {
            game_status.ended = true;
        } 
        if(_nextMessageTimer != null) {
            clearTimeout(_nextMessageTimer);
        }
        _nextMessageTimer = null;

    }
    

    


    $(document).ready(function() {


        $('#modal-close-button, .modal-play-button').on('click', function(){
            $("#message-modal-box-wrapper").fadeOut(200,function(){
                $("#message-modal-box, #start-message-modal-box, #message-final-box").css("display","none");
                if($game.ended == true) {
                    onGameEnded();
                } else if($game.gameOver) {
                    $game.ended = false;
                    $game.gameOver = false;
                    $game.restart();
                } else {
                    $game.paused = false;

                }
            });
        });
        $('#modal-final-close-button').on('click', function() {
            $("#message-modal-box-wrapper").fadeOut(200,function(){
                $("#message-modal-box, #start-message-modal-box, #message-final-box").css("display","none");
                $game.paused = false;
                
            });
        });
        
        $(window).resize(function(){
            $('#pile').height($('#puzzle-image').height());
            $('#puzzle-image').snapPuzzle('refresh');
        });

        $game = new GameController('#puzzle-canvas');


        


        
    });
})(jQuery);