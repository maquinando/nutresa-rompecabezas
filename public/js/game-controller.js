

var messages = {
  start: {
    title: null,
    btn: '¡A jugar!',
    msg: 'En este juego encontrarás una serie de rompecabezas que debes armar ubicando las fichas correctamente. Comienza a jugar y conoce más sobre cada uno de los procesos.'
  },
  end: {
    title: '¡MUY BIEN!',
    btn: null,
    msg: 'Lograste completar de manera correcta los procesos transversales del Negocio.',
  },
  pause: {
    title: null,
    btn: null,
    msg: '<b>¡Juego en pausa!</b>.',
  }
};
var gameEnded = false;
var isComplete = false;
var paused = false;
var messageCallback = null;

var selector = '#puzzle-canvas-wrapper';
var currentPuzzle = parseInt(localStorage.getItem('currentPuzzle'));
var puzzleCount = 6;
if(!currentPuzzle /*&& currentPuzzle > puzzleCount*/) currentPuzzle = 1;

var puzzleColumns = 6;
var puzzleRows = 4;
(function($, PuzzleController) {
  $(document).ready(function() {
    var game = new PuzzleController();

    game.selector = selector;
    game.callback = function() {
      showFullImage(function() {
        nextPuzzle();
      });
    };

    function nextPuzzle() {

      if(currentPuzzle >= puzzleCount) {
        showMessage(messages.end, onGameEnded);
        localStorage.removeItem('currentPuzzle');
        gameEnded = true;
        return;
      }

      currentPuzzle++;
      loadPuzzle(currentPuzzle);

    }
    function loadPuzzle(number) {
      if(number < 1 || number > puzzleCount)
        number = 1;
      currentPuzzle = number;
      var isComplete = false;
      $(selector).empty();
      game.startPuzzle('img/puzzle/' + currentPuzzle, currentPuzzle);

      localStorage.setItem('currentPuzzle', currentPuzzle);
    }
    function organizeShapes() {

      $('#helper').empty();
      $(selector).find('.piece').each(function() {
        var helperClass = this.className.split(' ')[2].replace('piece', 'helper');
        var $helper = $('<div>').addClass(piecesSize).addClass('helper').addClass(helperClass);
        var posX = $(this).data('posx');
        var posY = $(this).data('posy');
        var width = $(this).width();
        var height = $(this).height();
        var backgroundImage = $(this).css('background-image');
        var backgroundPos = $(this).css('background-position');

        $helper.css('background-image', backgroundImage);
        $helper.css('background-position', backgroundPos);
        $helper.css('width', width-2);
        $helper.css('height', height-2);

        $wrapper = $('<div class="helper-wrapper">');
        $wrapper.addClass(piecesSize).addClass(helperClass);
        $wrapper.css('width', width);
        $wrapper.css('height', height);
        $wrapper.css('left', posX);
        $wrapper.css('top', posY);

        $wrapper.append($helper);
        $('#helper').append($wrapper);
      })
      $('#pile').empty();
      $(selector)
      .find('.piece')
      .sort(function(a, b){return 0.5 - Math.random()})
      .each(function() {
        var piece = $(this).clone();
        piece.removeClass("ui-draggable");
        piece.addClass("piece-mock");
        piece.removeAttr("id");

        piece.attr('target', '#' + $(this).attr('id'));

        $('#pile').append(piece);
        $(this).addClass('hidden');

        piece.on('click mousedown touchstart', function(){
          var _target = $(this).attr('target');
          var piece = $(_target);
          $(this).remove();
          piece.removeClass('hidden');
        })

      });
    }
    $('#show-full-image-button').on('click', function(e) {
      showFullImage();
      e.preventDefault();
    });
    function showFullImage(callback) {
      $.colorbox({
        href: 'img/puzzle/' + currentPuzzle + '/puzzle.jpg',
        height: "80%",
        width: "80%",
        onComplete: function() {
        // $('.cboxPhoto').zoom({url: 'img/full/puzzle_' + currentPuzzle + '.jpg'});
        $('#cboxLoadedContent img')
        .wrap('<span style="display:inline-block"></span>').
        css('display', 'block').parent()
        .zoom({
          url: 'img/puzzle/' + currentPuzzle + '/full.jpg'
        });
      },
      onClosed: function() {
        if( callback ) callback();
      }
    });
    }
    function pause(doNotShowMessage) {
      paused = true;
      if(!doNotShowMessage) showMessage(messages.pause);
    }
    function setupModals() {
      $('#modal-close-button, .modal-play-button').on('click', function(){
        $("#message-modal-box-wrapper").fadeOut(200,function(){
          $("#message-modal-box, #start-message-modal-box, #message-final-box").css("display","none");
          paused = false;
          if(messageCallback) messageCallback();
          if(gameEnded) {
            loadPuzzle(currentPuzzle);
          }
        });
      });
      $('#modal-final-close-button').on('click', function() {
        $("#message-modal-box-wrapper").fadeOut(200,function(){
          $("#message-modal-box, #start-message-modal-box, #message-final-box").css("display","none");
          paused = false;
          if(messageCallback) messageCallback();
        });
      });
      $(window).resize(function(){

      });
      showMessage(messages.start);
    }

    function showMessage(msg) {
      if($('#message-final-box').is(':visible') || $('#start-message-modal-box').is(':visible'))
        return;
      $modalBox = $('#message-modal-box');
      if(!msg.title || msg.title == '') {
        $modalBox.find('#title-word').hide();
      } else {
        $modalBox.find('#title-word').show().html(msg.title);
      }
      if(!msg.msg || msg.msg == '') {
        $modalBox.find('#related-message').hide();
      } else {
        $modalBox.find('#related-message').show().html(msg.msg);
      }
      if(!msg.btn || msg.btn == '') {
        $modalBox.find('#related-button').hide();
      } else {
        $modalBox.find('#related-button').show().html(msg.btn);
      }

      $('#message-modal-box').css('display', 'inline-block');
      $('#message-modal-box-wrapper').fadeIn('slow');
    }
    function onGameEnded() {
    // Callback para cuando el juego termine
  }
  setupModals();
  loadPuzzle(currentPuzzle);
  // nextPuzzle();
})
})(jQuery, PuzzleController)

