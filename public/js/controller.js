jQuery(document).ready(function($) {
	var isComplete = false;
	var currentPuzzle = 0;
	var piecesSize = 'big';
	function nextPuzzle() {
		var isComplete = false;
		currentPuzzle++;
		$("#puzzle-wrapper").empty();
		var _src = 'img/puzzle_' + currentPuzzle + '.jpg';
		var puzzle = jqJigsawPuzzle.createPuzzleFromURL("#puzzle-wrapper", _src, {
			piecesSize: piecesSize, // normal, big o small
			borderWidth: 4, // el borde al rededor de cada pieza
			shuffle: { // define el área donde deben quedar las piezas.
				rightLimit : 230,
				leftLimit : -580,
				bottomLimit : 0,
				topLimit : 0 
			}, 
			callback: function() {

				showFullImage(function() {
					nextPuzzle();
				}); 
				showPuzzleHelpers();
			},
			container: "#puzzle-wrapper"
		});
		puzzle.find('[class^="piece"]').each(function() {
			var helperClass = this.className.replace('piece', 'helper');
			var $helper = $('<div>').addClass('newClass');
			$('#puzzle-pieces-helper').append($helper);
		})

		console.log(puzzle);
	}
	$('#show-full-image-button').on('click', function(e) {
		showFullImage();
		e.preventDefault();
	});
	function showPuzzleHelpers() {
		$('#puzzle-pieces-helper').find('.helper').css('display', 'block');
		$('#puzzle-pieces-helper').fadeIn('slow');
	}


	function showFullImage(callback) {
		$.colorbox({
			href: 'img/puzzle_' + currentPuzzle + '.jpg', 
			height: "80%", 
			width: "80%",
			onComplete: function() {
				// $('.cboxPhoto').zoom({url: 'img/full/puzzle_' + currentPuzzle + '.jpg'});
				$('#cboxLoadedContent img')
				.wrap('<span style="display:inline-block"></span>').
				css('display', 'block').parent()
				.zoom({
					url: 'img/full/puzzle_' + currentPuzzle + '.jpg'
				});
			}, 
			onClosed: function() {
				if( callback ) callback();
			}
		});
	}
	nextPuzzle();
});
