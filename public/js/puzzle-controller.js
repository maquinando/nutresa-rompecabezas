var PuzzleController = function() {
  this.selector = '#puzzle-canvas-wrapper';
  this.pileSelector = '#pile';
  this.imgLocation = 'img/puzzle/';
  this.pieceImgPrefix = '-';
  this.puzzleColumns = 6;
  this.puzzleRows = 4;
  this.fixedCount = 0;
  this.currentPuzzle = 1;
  this.callback = function() {
    console.log('Game has finnished.');
  }
}
PuzzleController.prototype.startPuzzle = function(location, currentPuzzle) {
  if(typeof currentPuzzle !== 'undefined')
    this.currentPuzzle = currentPuzzle;
  this.fixedCount = 0;
  if(location)
    this.imgLocation = location;
  var pieceWidth   =  (($(this.selector).width() / this.puzzleColumns) / 100) * 170;
  var pieceOffsetX =  (($(this.selector).width() / this.puzzleColumns) / 100) * 35;
  var pieceHeight  =  (($(this.selector).height() / this.puzzleRows) / 100) * 170;
  var pieceOffsetY =  (($(this.selector).height() / this.puzzleRows) / 100) * 35;


  $puzzleWrapper = $('<div></div>');
  $puzzleWrapper.addClass('puzzle-wrapper');
  $puzzleWrapper.css('background-image', 'url('+location + '/puzzle.jpg'+')');

  $helperWrapper = $('<div></div>');
  $helperWrapper.addClass('helper-wrapper');

  $draggableWrapper = $('<div></div>');
  $draggableWrapper.addClass('pieces-wrapper');

  var pileElements = [];

  var row = 0;
  var column = 0;

  var _this = this;

  for (var i = 0; i < this.puzzleColumns*this.puzzleRows; i++) {
    var x = -(pieceOffsetX * ((column * 2) + 1)) + (column * pieceWidth);
    var y = -(pieceOffsetY * ((row * 2) + 1)) + (row * pieceHeight);
    var imgLocation = this.imgLocation + '/pieces/proceso ' + (this.currentPuzzle) + '-' + (i < 9? '0':'') + (i + 1) + '.png';

    $draggable = $('<div></div>');
    $draggable.addClass('draggable');
    $draggable.attr('id', 'piece_' + (i + 1));
    $draggable.data('piece', (i + 1));
    $draggable.css('width', pieceWidth);
    $draggable.css('height', pieceHeight);
    $draggable.css('puzzle-top', y);
    $draggable.css('puzzle-left', x);
    $draggable.css('display', 'none');

    $helper = $('<div></div>');
    $helper.addClass('helper');
    $helper.attr('id', 'helper_' + (i + 1));
    $helper.data('piece', (i + 1));
    $helper.css('top', y);
    $helper.css('left', x);
    $helper.css('width', pieceWidth);
    $helper.css('height', pieceHeight);

    $piece = $('<div></div>');
    $piece.addClass('piece');
    $piece.css('background-image', 'url("' + imgLocation + '")');

    $pilePiece = $('<div></div>');
    $pilePiece.addClass('pile-piece');
    $pilePiece.attr('id', 'pile-piece_' + (i + 1));
    $pilePiece.data('piece', (i + 1));
    $pilePiece.append('<img src="' + imgLocation + '">');



    $helper.append($piece.clone());
    $draggable.append($piece.clone());

    $helperWrapper.append($helper);
    $draggableWrapper.append($draggable);

    pileElements.push($pilePiece);

    column ++;
    if(column >= this.puzzleColumns) {
      column = 0;
      row ++;
    }

  }
  pileElements.sort(function(a, b){return 0.5 - Math.random()});
  for (var i = 0; i < pileElements.length; i++) {
    $(this.pileSelector).append(pileElements[i]);
  }

  if(!$(this.selector).hasClass('puzzle'))
    $(this.selector).addClass('puzzle');

  $puzzleWrapper.append($helperWrapper);
  $puzzleWrapper.append($draggableWrapper);
  $(this.selector).html($puzzleWrapper);

  $('.draggable').draggable({
    handle: "p",
    stop: function(event, ui) {
      var piece = parseInt($(this).attr('id').replace('piece_', ''));

      var p_left = $(this).offset().left;
      var p_top = $(this).offset().top;

      var h_left = $('#helper_' + piece).offset().left;
      var h_top = $('#helper_' + piece).offset().top;

      var d = Math.sqrt( Math.pow(p_left - h_left, 2) + Math.pow(p_top - h_top, 2) );
      if(d < 20) {
        _this.fixedCount ++;
        $(this).offset({ left: h_left, top: h_top});
        $(this).css('z-index', 1);
        $(this).draggable( 'disable' );
        if(_this.fixedCount >= _this.puzzleRows * _this.puzzleColumns) {
          $(_this.selector).find('.helper-wrapper, .pieces-wrapper').fadeOut('slow', function() {
            window.setTimeout(function() {
              eval(_this.callback());
            }, 1500)
          });
        }
      }
    }
  });

  $('.pile-piece').on('touchstart mousedown', function(e) {
    e.preventDefault();
    var piece = $(this).data('piece');
    var $piece = $('#piece_' + piece);
    $piece.removeClass('locked');
    $piece.css('display', 'block');

    var helperOffsetX =  $(e.target).offset().left - ((pieceWidth - $(e.target).width()) / 2);
    var helperOffsetY =  $(e.target).offset().top - ((pieceHeight - $(e.target).height()) / 2);

    $piece.css('width', 50);
    $piece.css('height', 50);
    $piece.css('display', 'block');
    $piece.css('opacity', 0);
    $piece.css('z-index', 1000);


    $piece.offset( { top: helperOffsetY, left: helperOffsetX } );


    $piece.animate({
      opacity: 1,
      width: pieceWidth,
      height: pieceHeight
    }, 800, function() {
      // Animation complete.
    });

    e.type = "mousedown.draggable";
    e.target = $piece[0];

    $piece.trigger(e);
    $(this).fadeOut();
  });
};
